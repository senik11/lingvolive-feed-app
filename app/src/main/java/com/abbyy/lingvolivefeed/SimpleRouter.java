package com.abbyy.lingvolivefeed;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.abbyy.lingvolivefeed.base.entity.Post;
import com.abbyy.lingvolivefeed.post.PostActivity;
import com.abbyy.lingvolivefeed.settings.SettingsActivity;

public class SimpleRouter {

    private final Context context;

    public SimpleRouter(Context context) {
        this.context = context;
    }

    public void navigateTo(Class<? extends Activity> activityClass) {
        Intent intent = new Intent(context, activityClass);
        context.startActivity(intent);
    }

    public void navigateToPostActvity(Post data) {
        Intent postActivityIntent = new Intent(context, PostActivity.class);
        postActivityIntent.putExtra(PostActivity.KEY_EXTRA_POST, data);
        context.startActivity(postActivityIntent);
    }

    public void navigateToSettingsActivity() {
        navigateTo(SettingsActivity.class);
    }
}
