package com.abbyy.lingvolivefeed.post;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.base.entity.Post;

public class PostActivity extends AppCompatActivity {

    public static final String KEY_EXTRA_POST = "extra-post";
    public static final String KEY_STATE_POST_DATA = "state-post-data";

    private Toolbar toolbar;
    private View postDetails;
    private View postDetailsAccent;
    private TextView postDetailsLabel;
    private TextView postDetailsSummary;
    private TextView postDetailsAccentLabel;
    private TextView postDetailsAccentSummary;

    private Post postData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(getResources().getDimension(R.dimen.toolbar_elevation_default));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            postData = getIntent().getParcelableExtra(KEY_EXTRA_POST);
        } else {
            postData = savedInstanceState.getParcelable(KEY_STATE_POST_DATA);
        }
        postDetails = findViewById(R.id.post_details);
        postDetailsAccent = findViewById(R.id.post_details_accent);
        postDetailsLabel = (TextView) postDetails.findViewById(R.id.post_details_label);
        postDetailsSummary = (TextView) postDetails.findViewById(R.id.post_details_summary);
        postDetailsAccentLabel = (TextView) findViewById(R.id.post_details_accent_label);
        postDetailsAccentSummary = (TextView) findViewById(R.id.post_details_accent_summary);

        Post.Type postType = postData.getType();
        String postAuthorName = postData.getAuthorFirstName() + " " + postData.getAuthorLastName();
        if (postType == Post.Type.TRANSLATION_REQUEST) {
            String accentLabelText = getString(R.string.post_type_request_by, postAuthorName);

            postDetails.setVisibility(View.GONE);
            postDetailsAccentLabel.setText(accentLabelText);
            postDetailsAccentSummary.setText(postData.getHeading());
        } else if (postType == Post.Type.FREE) {
            String accentLabelText = getString(R.string.post_type_free_by, postAuthorName);

            postDetails.setVisibility(View.GONE);
            postDetailsAccentLabel.setText(accentLabelText);
            postDetailsAccentSummary.setText(postData.getMessage());
        } else if (postType == Post.Type.USER_TRANSLATION) {
            String detailsAccentText = getString(R.string.post_type_request);
            String detailsLabelText = getString(R.string.post_type_translation_by, postAuthorName);

            postDetailsLabel.setText(detailsLabelText);
            postDetailsSummary.setText(postData.getTranslation());
            postDetailsAccentLabel.setText(detailsAccentText);
            postDetailsAccentSummary.setText(postData.getHeading());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_STATE_POST_DATA, postData);
        super.onSaveInstanceState(outState);
    }
}
