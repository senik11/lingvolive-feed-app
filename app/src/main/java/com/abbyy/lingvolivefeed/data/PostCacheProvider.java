package com.abbyy.lingvolivefeed.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

public class PostCacheProvider extends ContentProvider {

    private static final String LOG_TAG = PostCacheProvider.class.getSimpleName();
    
    private SQLiteOpenHelper dbHelper;
    private SQLiteQueryBuilder dbQueryBuilder;
    private UriMatcher matcher;

    static class Match {
        public static final int NO_MATCH = -1;
        public static final int POST_ONE = 0;
        public static final int POST_MANY = 1;
    }

    @Override
    public boolean onCreate() {
        this.dbHelper = new PostCacheDbHelper(getContext());
        this.dbQueryBuilder = new SQLiteQueryBuilder();
        this.dbQueryBuilder.setTables(
                PostCacheContract.PostEntry.TABLE_NAME
        );
        this.matcher = createMatcher();
        return true;
    }

    private static UriMatcher createMatcher() {
        UriMatcher matcher = new UriMatcher(Match.NO_MATCH);
        matcher.addURI(PostCacheContract.CONTENT_AUTHORITY, PostCacheContract.PATH_POSTS, Match.POST_MANY);
        matcher.addURI(PostCacheContract.CONTENT_AUTHORITY, PostCacheContract.PATH_POSTS + "/#", Match.POST_ONE);
        return matcher;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final int match = matcher.match(uri);
        if (match == Match.POST_MANY) {
            Cursor result = dbHelper.getReadableDatabase().query(
                    PostCacheContract.PostEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArgs,
                    null,
                    null,
                    sortOrder
            );
            result.setNotificationUri(getContext().getContentResolver(), uri);
            return result;
        }
        throw new UnsupportedOperationException("Can't match uri: " + uri);
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Uri resultUri = null;
        final int match = matcher.match(uri);
        if (match == Match.POST_MANY) {
            final long last = dbHelper.getWritableDatabase().insertWithOnConflict(
                    PostCacheContract.PostEntry.TABLE_NAME,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_IGNORE
            );
            if (last > -1) {
                resultUri = PostCacheContract.buildPostPath(last);
            }
        } else {
            throw new UnsupportedOperationException("Can't match uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return resultUri;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] valuesArray) {
        final int match = matcher.match(uri);
        Log.d(LOG_TAG, "bulkInsert");
        int count = 0;
        if (match == Match.POST_MANY) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                for (ContentValues values : valuesArray) {
                    long id = db.insertWithOnConflict(
                            PostCacheContract.PostEntry.TABLE_NAME,
                            null,
                            values,
                            SQLiteDatabase.CONFLICT_IGNORE
                    );
                    count += id != -1 ? 1 : 0;
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
            if (count > 0) {
                getContext().getContentResolver().notifyChange(uri, null);
            }
            return count;
        } else {
            return super.bulkInsert(uri, valuesArray);
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final int match = matcher.match(uri);
        int deletes;
        if (selection == null) {
            selection = "1";
        }
        if (match == Match.POST_MANY) {
            deletes = dbHelper.getWritableDatabase().delete(
                    PostCacheContract.PostEntry.TABLE_NAME,
                    selection,
                    selectionArgs
            );
        } else {
            throw new UnsupportedOperationException("Can't match uri: " + uri);
        }
        if (deletes > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return deletes;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
