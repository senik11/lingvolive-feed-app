package com.abbyy.lingvolivefeed.data.service;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.base.entity.Post;
import com.abbyy.lingvolivefeed.data.PostCacheContract;
import com.abbyy.lingvolivefeed.data.net.PostDownloader;
import com.abbyy.lingvolivefeed.data.net.UrlDownloaderImpl;

import java.util.ArrayList;
import java.util.List;

public class FeedSyncService extends IntentService {

    private static final String LOG_TAG = FeedSyncService.class.getSimpleName();

    public static final String SERVICE_STATUS_INTENT = "service-status";

    public static class Extra {
        public static final String STATUS = "status";
        public static final String UPDATES_COUNT = "updates-count";
        public static final String FAILURE_CODE = "failure-code";
    }

    public static class Status {
        public static final int PROCESS = 0;
        public static final int SUCCESS = 1;
        public static final int FAILURE = 2;
    }

    public static class FailureCode {
        public static final int NO_CONNECTION = 0;
        public static final int API_UNREACHABLE = 1;
        public static final int UNKNOWN = 2;
    }

    private LocalBroadcastManager broadcastManager;
    private ConnectivityManager connectivityManager;

    public FeedSyncService() {
        super(LOG_TAG + "-lingvolivefeed");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(LOG_TAG, "Starting sync...");

        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        connectivityManager = ((ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE));
        if (!isInternetAvailable()) {
            Log.d(LOG_TAG, "Internet connection unavailable, aborting");

            Bundle failExtra = new Bundle();
            failExtra.putInt(Extra.FAILURE_CODE, FailureCode.NO_CONNECTION);
            notifyWatchers(Status.FAILURE, failExtra);
        } else {
            Log.d(LOG_TAG, "Internet connection available, resuming sync...");

            notifyWatchers(Status.PROCESS);
            try {
                int updates = updateCache();

                Bundle successExtra = new Bundle();
                successExtra.putInt(Extra.UPDATES_COUNT, updates);
                notifyWatchers(Status.SUCCESS, successExtra);

            } catch (PostDownloader.DownloadException e) {
                Log.e(LOG_TAG, "Error while syncing", e);

                Bundle failExtra = new Bundle();
                failExtra.putInt(Extra.FAILURE_CODE, FailureCode.API_UNREACHABLE);
                notifyWatchers(Status.FAILURE, failExtra);
            }
        }
    }

    private int updateCache() throws PostDownloader.DownloadException {
        PostDownloader downloader = new PostDownloader(getApplicationContext(), new UrlDownloaderImpl());
        List<Post> posts = downloader.getPosts(10);
        List<ContentValues> postValues = new ArrayList<>();

        for (Post post : posts) {
            postValues.add(post.toContentValues());
        }

        ContentValues[] cvPostArray = new ContentValues[postValues.size()];
        postValues.toArray(cvPostArray);

        ContentResolver resolver = getApplicationContext().getContentResolver();

        int postsCount = resolver.bulkInsert(
                PostCacheContract.PostEntry.CONTENT_URI,
                cvPostArray
        );

        int maxCacheSize = getApplicationContext().getResources().getInteger(R.integer.max_cache_size);
        int postsDeleted = truncateCache(resolver, maxCacheSize);

        Log.d(LOG_TAG, String.format("Rows inserted: %d, rows deleted: %d", postsCount, postsDeleted));

        return postsCount;
    }

    private int truncateCache(ContentResolver resolver, int maxCacheSize) {
        int deletes = 0;
        if (maxCacheSize > -1) {
            String selection = PostCacheContract.PostEntry._ID + " IN" +
                    " (SELECT " + PostCacheContract.PostEntry._ID +
                    " FROM " + PostCacheContract.PostEntry.TABLE_NAME +
                    " ORDER BY " + PostCacheContract.PostEntry.TIMESTAMP_COLUMN + " DESC" +
                    " LIMIT -1 OFFSET ?)";
            deletes = resolver.delete(
                    PostCacheContract.PostEntry.CONTENT_URI,
                    selection,
                    new String[]{String.valueOf(maxCacheSize)}
            );
        }
        return deletes;
    }

    private boolean isInternetAvailable() {
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void notifyWatchers(int status) {
        notifyWatchers(status, Bundle.EMPTY);
    }

    private void notifyWatchers(int status, Bundle extras) {
        broadcastManager.sendBroadcast(
                new Intent(SERVICE_STATUS_INTENT).putExtra(Extra.STATUS, status).putExtras(extras)
        );
    }

}
