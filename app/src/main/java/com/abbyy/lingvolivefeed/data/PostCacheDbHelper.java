package com.abbyy.lingvolivefeed.data;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PostCacheDbHelper extends SQLiteOpenHelper {

    public static final String DB_FILENAME = "post_cache.db";
    public static final int DB_VERSION = 3;

    public PostCacheDbHelper(Context context) {
        super(context, DB_FILENAME, null, DB_VERSION, new DropDatabaseOnErrorHandler());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cleanupDb(db);
    }

    protected static void createTables(SQLiteDatabase db) {
        String queryStrPosts = String.format(
                "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "%s INTEGER NOT NULL UNIQUE, " +
                        "%s TEXT NOT NULL," +
                        "%s TEXT," +
                        "%s TEXT, " +
                        "%s VARCHAR(255) NOT NULL, " +
                        "%s DATETIME NOT NULL, " +
                        "%s VARCHAR(255) NOT NULL, " +
                        "%s VARCHAR(255) NOT NULL, " +
                        "%s VARCHAR(255));",
                PostCacheContract.PostEntry.TABLE_NAME,
                PostCacheContract.PostEntry._ID,
                PostCacheContract.PostEntry.DB_ID_COLUMN,
                PostCacheContract.PostEntry.HEADING_COLUMN,
                PostCacheContract.PostEntry.TRANSLATION_COLUMN,
                PostCacheContract.PostEntry.MESSAGE_COLUMN,
                PostCacheContract.PostEntry.TYPE_COLUMN,
                PostCacheContract.PostEntry.TIMESTAMP_COLUMN,
                PostCacheContract.PostEntry.AUTHOR_ID_COLUMN,
                PostCacheContract.PostEntry.AUTHOR_NAME_FIRST_COLUMN,
                PostCacheContract.PostEntry.AUTHOR_NAME_LAST_COLUMN
        );
        db.beginTransaction();
        db.execSQL(queryStrPosts);
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    protected static void dropTables(SQLiteDatabase db) {
        String dropQueryFormat = "DROP TABLE IF EXISTS %s";
        db.beginTransaction();
        db.execSQL(String.format(dropQueryFormat, PostCacheContract.PostEntry.TABLE_NAME));
        db.setTransactionSuccessful();
        db.endTransaction();
    }

    protected static void cleanupDb(SQLiteDatabase db) {
        dropTables(db);
        createTables(db);
    }

    public static class DropDatabaseOnErrorHandler implements DatabaseErrorHandler {
        @Override
        public void onCorruption(SQLiteDatabase db) {
            cleanupDb(db);
        }
    }
}
