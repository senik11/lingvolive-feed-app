package com.abbyy.lingvolivefeed.data.net;

import com.abbyy.lingvolivefeed.data.net.base.UrlDownloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class UrlDownloaderImpl implements UrlDownloader {

    @Override
    public String load(URL url) throws IOException {
        StringBuilder builder = new StringBuilder();
        HttpURLConnection connection = ((HttpURLConnection) url.openConnection());
        connection.setReadTimeout(2000);
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), Charset.forName("utf-8")))
        ) {
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return builder.toString();
    }

}
