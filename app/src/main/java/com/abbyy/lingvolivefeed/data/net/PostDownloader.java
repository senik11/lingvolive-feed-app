package com.abbyy.lingvolivefeed.data.net;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.base.StaticUtils;
import com.abbyy.lingvolivefeed.base.entity.Post;
import com.abbyy.lingvolivefeed.data.net.base.UrlDownloader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostDownloader {

    private static final String LOG_TAG = PostDownloader.class.getSimpleName();

    private UrlDownloader downloader;
    private Context context;

    public PostDownloader(Context context, UrlDownloader downloader) {
        this.downloader = downloader;
        this.context = context;
    }

    public List<Post> getPosts(final int postCount) throws DownloadException {
        Uri uri = Uri.parse(context.getString(R.string.url_feed))
                .buildUpon()
                .appendQueryParameter("pageSize", "10")
                .build();
        Log.d(LOG_TAG, "getPosts: uri " + uri.toString());
        List<Post> posts = new ArrayList<>(postCount);
        try {
            String raw = downloader.load(new URL(uri.toString()));
            posts.addAll(deserialize(raw));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Invalid feed url provided", e);
        } catch (JSONException e) {
            throw new DownloadException("Deserializing failure", e);
        }
        return posts;
    }

    private List<Post> deserialize(String raw) throws JSONException {
        List<Post> result = new ArrayList<>(10);
        JSONObject parent = new JSONObject(raw);
        JSONArray posts = parent.getJSONArray("posts");
        for (int i = 0; i < posts.length(); i++) {
            JSONObject postObject = posts.getJSONObject(i);
            JSONObject postAuthorObject = postObject.getJSONObject("author");

            int postDbId = postObject.getInt("postDbId");
            String postTypeStr = postObject.getString("postType");
            String postHeading = postObject.has("heading") ? postObject.getString("heading") : "";
            String postTranslation = postObject.has("translation") ? postObject.getString("translation") : "";
            String postMessage = postObject.has("message") ? postObject.getString("message") : "";
            String postTimestamp = postObject.getString("timestamp");

            String authorDbId = postAuthorObject.getString("id");
            String authorFirstName = postAuthorObject.getString("name");
            String authorLastName = postAuthorObject.getString("familyName");

            Post.Type postType = Post.Type.forString(postTypeStr);
            Date postTimestampDate = null;
            try {
                postTimestampDate = StaticUtils.parseDate(postTimestamp);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Post post = new Post(postDbId, authorDbId, authorFirstName, authorLastName, postHeading, postTranslation, postMessage, postType, postTimestampDate);

            result.add(post);
        }

        return result;
    }

    public static class DownloadException extends Exception {
        public DownloadException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
