package com.abbyy.lingvolivefeed.data.net.base;

import java.io.IOException;
import java.net.URL;

public interface UrlDownloader {

    String load(URL url) throws IOException;

}
