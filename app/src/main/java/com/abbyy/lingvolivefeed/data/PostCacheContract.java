package com.abbyy.lingvolivefeed.data;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

public class PostCacheContract {

    public static final String CONTENT_SCHEME = "content://";

    public static final String CONTENT_AUTHORITY = "com.abbyy.lingvolivefeed";

    public static final Uri BASE_URI = Uri.parse(CONTENT_SCHEME + CONTENT_AUTHORITY);

    public static final String PATH_POSTS = "posts";

    public static Uri buildPostPath(long id) {
        return ContentUris.withAppendedId(PostEntry.CONTENT_URI, id);
    }

    public static String retrievePath(Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static class PostEntry implements BaseColumns {
        public static final String TABLE_NAME = "posts";
        public static final String DB_ID_COLUMN = "db_id";
        public static final String HEADING_COLUMN = "heading";
        public static final String TRANSLATION_COLUMN = "translation";
        public static final String MESSAGE_COLUMN = "message";
        public static final String AUTHOR_ID_COLUMN = "author_id";
        public static final String AUTHOR_NAME_FIRST_COLUMN = "author_name";
        public static final String AUTHOR_NAME_LAST_COLUMN = "author_name_last";
        public static final String TYPE_COLUMN = "type";
        public static final String TIMESTAMP_COLUMN = "timestamp";

        public static final int COL_ID = 0;
        public static final int COL_DB_ID = 1;
        public static final int COL_HEADING = 2;
        public static final int COL_TRANSLATION = 3;
        public static final int COL_MESSAGE = 4;
        public static final int COL_TYPE = 5;
        public static final int COL_TIMESTAMP = 6;
        public static final int COL_AUTHOR_ID = 7;
        public static final int COL_AUTHOR_NAME_FIRST = 8;
        public static final int COL_AUTHOR_NAME_LAST = 9;

        public static final Uri CONTENT_URI = BASE_URI.buildUpon().appendPath(PATH_POSTS).build();
    }
}
