package com.abbyy.lingvolivefeed.data.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class FeedSyncServiceStatusReceiver extends BroadcastReceiver {

    private final StateListener stateListener;
    private final Context context;

    public FeedSyncServiceStatusReceiver(StateListener stateListener, Context context) {
        this.stateListener = stateListener;
        this.context = context;
    }

    public void subscribe() {
        IntentFilter filter = new IntentFilter(FeedSyncService.SERVICE_STATUS_INTENT);
        LocalBroadcastManager.getInstance(context).registerReceiver(this, filter);
    }

    public void unsubscribe() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        int serviceStatus = intent.getIntExtra(FeedSyncService.Extra.STATUS, -1);
        switch (serviceStatus) {
            case FeedSyncService.Status.PROCESS: {
                stateListener.onUpdateProcess();
                break;
            }
            case FeedSyncService.Status.SUCCESS: {
                int updatesCount = intent.getIntExtra(FeedSyncService.Extra.UPDATES_COUNT, 0);
                stateListener.onUpdateSuccess(updatesCount);
                break;
            }
            case FeedSyncService.Status.FAILURE: {
                int failCode = intent.getIntExtra(FeedSyncService.Extra.FAILURE_CODE, 0);
                stateListener.onUpdateFailure(failCode);
                break;
            }
        }
    }

    public interface StateListener {
        void onUpdateProcess();
        void onUpdateSuccess(int updates);
        void onUpdateFailure(int status);
    }
}
