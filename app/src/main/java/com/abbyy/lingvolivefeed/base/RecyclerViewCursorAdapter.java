package com.abbyy.lingvolivefeed.base;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.v7.widget.RecyclerView;

import com.abbyy.lingvolivefeed.data.PostCacheContract;

public abstract class RecyclerViewCursorAdapter<T extends RecyclerView.ViewHolder> extends
        RecyclerView.Adapter<T> {

    private Cursor cursor;
    private DataSetObserver cursorObserver;

    public RecyclerViewCursorAdapter(Cursor cursor) {
        this.cursor = cursor;
        this.cursorObserver = new NotifyOnChangeObserver();
        setHasStableIds(true);
    }

    protected abstract T onBindViewHolder(T holder, Cursor cursor);

    @Override
    public void onBindViewHolder(T holder, int position)
    {
        if(!cursor.moveToPosition(position)) {
            throw new IllegalStateException(String.format("Can't reach position: %d", position));
        }
        onBindViewHolder(holder, cursor);
    }

    @Override
    public long getItemId(int position) {
        if (cursor == null || !cursor.moveToPosition(position)) {
            return RecyclerView.NO_ID;
        }
        return cursor.getLong(PostCacheContract.PostEntry.COL_ID);
    }

    @Override
    public int getItemCount() {
        return cursor != null ? cursor.getCount() : 0;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor newCursor) {
        if (cursor == newCursor) return;
        if (cursor != null) {
            if (cursorObserver != null) {
                cursor.unregisterDataSetObserver(cursorObserver);
            }
            cursor.close();
        }
        cursor = newCursor;
        if (cursor != null) {
            if (cursorObserver != null) {
                cursor.registerDataSetObserver(cursorObserver);
            }
        }
        notifyDataSetChanged();
    }


    private class NotifyOnChangeObserver extends DataSetObserver {
        @Override
        public void onChanged() {
            super.onChanged();
            notifyDataSetChanged();
        }

        @Override
        public void onInvalidated() {
            super.onInvalidated();
            notifyDataSetChanged();
        }
    }
}
