package com.abbyy.lingvolivefeed.base;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StaticUtils {

    private static final String[] acceptedDateFormats = {
            "yyyy-MM-dd'T'kk:mm:ss.SSS",
            "yyyy-MM-dd'T'kk:mm:ss"
    };

    public static Date parseDate(String date) throws ParseException {
        Date result = null;
        try {
            result = new SimpleDateFormat(acceptedDateFormats[0]).parse(date);
        } catch (ParseException e) {
            result = new SimpleDateFormat(acceptedDateFormats[1]).parse(date);
        }
        return result;
    }

    public static String formatDate(Date date) {
        return new SimpleDateFormat(acceptedDateFormats[0]).format(date);
    }

}
