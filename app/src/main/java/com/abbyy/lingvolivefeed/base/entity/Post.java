package com.abbyy.lingvolivefeed.base.entity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.abbyy.lingvolivefeed.base.StaticUtils;
import com.abbyy.lingvolivefeed.data.PostCacheContract;

import java.text.ParseException;
import java.util.Date;

public class Post implements Parcelable {

    private static final String LOG_TAG = Post.class.getSimpleName();

    private static final String KEY_ID = "id";
    private static final String KEY_DB_ID = "dbId";
    private static final String KEY_AUTHOR_ID = "authorId";
    private static final String KEY_AUTHOR_FIRST_NAME=  "authorFirstName";
    private static final String KEY_AUTHOR_LAST_NAME = "authorLastName";
    private static final String KEY_HEADING=  "heading";
    private static final String KEY_TRANSLATION = "translation";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_TYPE = "type";
    private static final String KEY_TIMESTAMP = "timestamp";

    private int id;

    private int dbId;

    private String authorId;

    private String authorFirstName;

    private String authorLastName;

    private String heading;

    private String translation;

    private String message;

    private Type type;

    private Date timestamp;

    public Post(Parcel parcel) {
        Bundle parcelBundle = parcel.readBundle();
        id = parcelBundle.getInt(KEY_ID);
        dbId = parcelBundle.getInt(KEY_DB_ID);
        authorId = parcelBundle.getString(KEY_AUTHOR_ID);
        authorFirstName = parcelBundle.getString(KEY_AUTHOR_FIRST_NAME);
        authorLastName = parcelBundle.getString(KEY_AUTHOR_LAST_NAME);
        heading = parcelBundle.getString(KEY_HEADING);
        translation = parcelBundle.getString(KEY_TRANSLATION);
        message = parcelBundle.getString(KEY_MESSAGE);
        type = Type.forString(parcelBundle.getString(KEY_TYPE));
        timestamp = ((Date) parcelBundle.getSerializable(KEY_TIMESTAMP));
    }

    public Post(int dbId, String authorId, String authorFirstName, String authorLastName, String heading, String translation, String message, Type type, Date timestamp) {
        this.dbId = dbId;
        this.authorId = authorId;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.heading = heading;
        this.translation = translation;
        this.message = message;
        this.type = type;
        this.timestamp = timestamp;
    }

    private Post(int id, int dbId, String authorId, String authorFirstName, String authorLastName, String heading, String translation, String message, Type type, Date timestamp) {
        this.id = id;
        this.dbId = dbId;
        this.authorId = authorId;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.heading = heading;
        this.translation = translation;
        this.message = message;
        this.type = type;
        this.timestamp = timestamp;
    }

    public static String getKeyId() {
        return KEY_ID;
    }

    public static String getKeyDbId() {
        return KEY_DB_ID;
    }

    public static String getKeyAuthorId() {
        return KEY_AUTHOR_ID;
    }

    public static String getKeyAuthorFirstName() {
        return KEY_AUTHOR_FIRST_NAME;
    }

    public static String getKeyAuthorLastName() {
        return KEY_AUTHOR_LAST_NAME;
    }

    public static String getKeyHeading() {
        return KEY_HEADING;
    }

    public static String getKeyTranslation() {
        return KEY_TRANSLATION;
    }

    public static String getKeyMessage() {
        return KEY_MESSAGE;
    }

    public static String getKeyType() {
        return KEY_TYPE;
    }

    public static String getKeyTimestamp() {
        return KEY_TIMESTAMP;
    }

    public int getId() {
        return id;
    }

    public int getDbId() {
        return dbId;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public String getHeading() {
        return heading;
    }

    public String getTranslation() {
        return translation;
    }

    public String getMessage() {
        return message;
    }

    public Type getType() {
        return type;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(PostCacheContract.PostEntry.DB_ID_COLUMN, dbId);
        values.put(PostCacheContract.PostEntry.HEADING_COLUMN, heading);
        values.put(PostCacheContract.PostEntry.TRANSLATION_COLUMN, translation);
        values.put(PostCacheContract.PostEntry.MESSAGE_COLUMN, message);
        values.put(PostCacheContract.PostEntry.TYPE_COLUMN, type.toString());
        values.put(PostCacheContract.PostEntry.AUTHOR_ID_COLUMN, authorId);
        values.put(PostCacheContract.PostEntry.AUTHOR_NAME_FIRST_COLUMN, authorFirstName);
        values.put(PostCacheContract.PostEntry.AUTHOR_NAME_LAST_COLUMN, authorLastName);
        values.put(
                PostCacheContract.PostEntry.TIMESTAMP_COLUMN,
                StaticUtils.formatDate(timestamp)
        );
        return values;
    }

    public static Post fromContentValues(ContentValues postCv, ContentValues userCv) {
        Date timestamp = new Date(0);
        try {
            timestamp = StaticUtils.parseDate(
                    userCv.getAsString(PostCacheContract.PostEntry.TIMESTAMP_COLUMN)
            );
        } catch (ParseException e) {
            Log.e(LOG_TAG, "fromContentValues: can't convert from CVs properly", e);
        }
        return new Post(
                postCv.getAsInteger(PostCacheContract.PostEntry._ID),
                postCv.getAsInteger(PostCacheContract.PostEntry.DB_ID_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.AUTHOR_ID_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.AUTHOR_NAME_FIRST_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.AUTHOR_NAME_LAST_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.HEADING_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.TRANSLATION_COLUMN),
                postCv.getAsString(PostCacheContract.PostEntry.MESSAGE_COLUMN),
                Type.forString(postCv.getAsString(PostCacheContract.PostEntry.TYPE_COLUMN)),
                timestamp
        );
    }

    public static Post fromCursor(Cursor cursor) {
        Date timestamp = new Date(0);
        try {
            timestamp = StaticUtils.parseDate(
                    cursor.getString(PostCacheContract.PostEntry.COL_TIMESTAMP)
            );
        } catch (ParseException e) {
            Log.e(LOG_TAG, "fromCursor: can't convert from cursor properly", e);
        }
        return new Post(
                cursor.getInt(PostCacheContract.PostEntry.COL_ID),
                cursor.getInt(PostCacheContract.PostEntry.COL_DB_ID),
                cursor.getString(PostCacheContract.PostEntry.COL_AUTHOR_ID),
                cursor.getString(PostCacheContract.PostEntry.COL_AUTHOR_NAME_FIRST),
                cursor.getString(PostCacheContract.PostEntry.COL_AUTHOR_NAME_LAST),
                cursor.getString(PostCacheContract.PostEntry.COL_HEADING),
                cursor.getString(PostCacheContract.PostEntry.COL_TRANSLATION),
                cursor.getString(PostCacheContract.PostEntry.COL_MESSAGE),
                Type.forString(cursor.getString(PostCacheContract.PostEntry.COL_TYPE)),
                timestamp
        );
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        Bundle parcelBundle = new Bundle();
        parcelBundle.putInt(KEY_ID, id);
        parcelBundle.putInt(KEY_DB_ID, dbId);
        parcelBundle.putString(KEY_AUTHOR_ID, authorId);
        parcelBundle.putString(KEY_AUTHOR_FIRST_NAME, authorFirstName);
        parcelBundle.putString(KEY_AUTHOR_LAST_NAME, authorLastName);
        parcelBundle.putString(KEY_HEADING, heading);
        parcelBundle.putString(KEY_TRANSLATION, translation);
        parcelBundle.putString(KEY_MESSAGE, message);
        parcelBundle.putString(KEY_TYPE, type.toString());
        parcelBundle.putSerializable(KEY_TIMESTAMP, timestamp);
        dest.writeBundle(parcelBundle);
    }

    public static final Parcelable.Creator<Post> CREATOR = new Parcelable.Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", dbId=" + dbId +
                ", authorId='" + authorId + '\'' +
                ", authorFirstName='" + authorFirstName + '\'' +
                ", authorLastName='" + authorLastName + '\'' +
                ", heading='" + heading + '\'' +
                ", translation='" + translation + '\'' +
                ", message='" + message + '\'' +
                ", type=" + type +
                ", timestamp=" + timestamp +
                '}';
    }

    public enum Type {
        TRANSLATION_REQUEST("TranslationRequest"),
        FREE("Free"),
        USER_TRANSLATION("UserTranslation");

        private String value;

        Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static Type forString(String value) {
            switch (value) {
                case "TranslationRequest" : return TRANSLATION_REQUEST;
                case "Free" : return FREE;
                case "UserTranslation" : return USER_TRANSLATION;
                default:
                    throw new IllegalArgumentException("No value found for " + value);
            }
        }
    }
}
