package com.abbyy.lingvolivefeed.feed;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.base.RecyclerViewCursorAdapter;
import com.abbyy.lingvolivefeed.base.entity.Post;
import com.abbyy.lingvolivefeed.data.PostCacheContract;

public class FeedAdapter extends RecyclerViewCursorAdapter<FeedElementViewHolder> {

    private static final String LOG_TAG = FeedAdapter.class.getSimpleName();

    private static final int UNKNOWN_VIEW_TYPE = -1;

    private OnPostClickListener postClickListener;

    public FeedAdapter(Cursor cursor) {
        super(cursor);
    }

    @Override
    protected FeedElementViewHolder onBindViewHolder(FeedElementViewHolder holder, final Cursor cursor) {
        final Post post = Post.fromCursor(cursor);

        holder.postContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (postClickListener != null) {
                    postClickListener.onPostClick(post);
                }
            }
        });

        switch (holder.viewType) {
            case R.layout.post_user_translation : {
                holder.postHeading.setText(post.getHeading());
                holder.postTranslation.setText(post.getTranslation());
                break;
            }
            case R.layout.post_translation_request : {
                holder.postHeading.setText(post.getHeading());
                break;
            }
            case R.layout.post_free_form : {
                holder.postMessage.setText(post.getMessage());
                break;
            }
        }
        return holder;
    }

    @Override
    public FeedElementViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View card = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
        return new FeedElementViewHolder(card, viewType);
    }

    @Override
    public int getItemViewType(int position) {
        if (getCursor() == null || !getCursor().moveToPosition(position)) {
            return UNKNOWN_VIEW_TYPE;
        }

        String postTypeStr = getCursor().getString(PostCacheContract.PostEntry.COL_TYPE);
        Post.Type postType = Post.Type.forString(postTypeStr);

        switch (postType) {
            case USER_TRANSLATION: return R.layout.post_user_translation;
            case TRANSLATION_REQUEST: return R.layout.post_translation_request;
            case FREE: return R.layout.post_free_form;
            default: return UNKNOWN_VIEW_TYPE;
        }
    }

    public void setPostClickListener(OnPostClickListener listener) {
        this.postClickListener = listener;
    }

    public void unsetPostClickListener() {
        this.postClickListener = null;
    }

    public interface OnPostClickListener {
        void onPostClick(Post post);
    }

}
