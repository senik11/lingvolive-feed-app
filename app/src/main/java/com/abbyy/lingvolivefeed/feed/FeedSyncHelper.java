package com.abbyy.lingvolivefeed.feed;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.data.service.FeedSyncService;

import java.util.Timer;
import java.util.TimerTask;

public class FeedSyncHelper {

    public static final String LOG_TAG = FeedSyncHelper.class.getSimpleName();

    private Timer timer;
    private final Context context;
    private final SharedPreferences preferences;

    public FeedSyncHelper(Context context) {
        this.context = context;
        this.preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void configureSync() {
        if (isAutoSyncEnabled()) {
            int periodMillis = getAutoSyncPeriodMillis();
            Log.d(LOG_TAG, "configureSync: auto sync configured with period " + periodMillis);
            timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            requestSync();
                        }
                    },
                    periodMillis,
                    periodMillis
            );
        }
    }

    public boolean isAutoSyncEnabled() {
        String autoUpdateKey = context.getString(R.string.prefs_sync_enabled_key);
        return preferences.getBoolean(autoUpdateKey, true);
    }

    public int getAutoSyncPeriodMillis() {
        String autoUpdateIntervalKey = context.getString(R.string.prefs_sync_interval_key);
        String autoUpdateIntervalDefaultStr = context.getResources().getString(R.string.prefs_sync_interval_default);

        String updateIntervalStr = preferences.getString(autoUpdateIntervalKey, autoUpdateIntervalDefaultStr);
        return Integer.parseInt(updateIntervalStr) * 60 * 1000;
    }

    public void disableSync() {
        if (timer != null) {
            timer.cancel();
        }
    }

    public void requestSync() {
        Intent feedUpdateIntent = new Intent(context, FeedSyncService.class);
        context .startService(feedUpdateIntent);
    }
}
