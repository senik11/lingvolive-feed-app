package com.abbyy.lingvolivefeed.feed;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

public class FeedView extends RecyclerView {

    private View stubView;
    private AdapterDataObserver dataObserver = new AdapterDataObserver() {
        @Override
        public void onChanged() {
            if (stubView == null) return;
            if (getAdapter() == null || getAdapter().getItemCount() == 0) {
                setVisibility(GONE);
                stubView.setVisibility(VISIBLE);
            } else {
                stubView.setVisibility(GONE);
                setVisibility(VISIBLE);
            }
        }
    };

    public FeedView(Context context) {
        super(context);
    }

    public FeedView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FeedView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void setAdapter(Adapter adapter) {
        Adapter old = getAdapter();
        if (dataObserver != null) {
            if (old != null) {
                old.unregisterAdapterDataObserver(dataObserver);
            }
            if (adapter != null) {
                adapter.registerAdapterDataObserver(dataObserver);
            }
        }
        super.setAdapter(adapter);
    }

    public void setStubView(View stubView) {
        this.stubView = stubView;
        dataObserver.onChanged();
    }
}
