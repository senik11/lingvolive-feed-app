package com.abbyy.lingvolivefeed.feed;

import android.content.Context;
import android.content.CursorLoader;

import com.abbyy.lingvolivefeed.data.PostCacheContract;

public class FeedLoader extends CursorLoader {

    public FeedLoader(Context context, boolean ascending) {
        super(
                context,
                PostCacheContract.PostEntry.CONTENT_URI,
                null,
                null,
                null,
                PostCacheContract.PostEntry.DB_ID_COLUMN + (ascending ? " ASC" : " DESC")
        );
    }

}
