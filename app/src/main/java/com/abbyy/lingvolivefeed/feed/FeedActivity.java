package com.abbyy.lingvolivefeed.feed;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.abbyy.lingvolivefeed.R;
import com.abbyy.lingvolivefeed.SimpleRouter;
import com.abbyy.lingvolivefeed.base.entity.Post;
import com.abbyy.lingvolivefeed.data.service.FeedSyncService;
import com.abbyy.lingvolivefeed.data.service.FeedSyncServiceStatusReceiver;

public class FeedActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Cursor>,
        SwipeRefreshLayout.OnRefreshListener,
        FeedSyncServiceStatusReceiver.StateListener,
        FeedAdapter.OnPostClickListener
{

    private static final String LOG_TAG = FeedActivity.class.getSimpleName();
    public static final int POSTS_LOADER = 1;

    private SimpleRouter router;

    private Toolbar toolbar;
    private Snackbar snackbar;

    private FeedAdapter feedAdapter;
    private SwipeRefreshLayout refreshLayout;

    private FeedView feedView;

    private FeedSyncHelper syncHelper;
    private FeedSyncServiceStatusReceiver feedSyncServiceStatusReceiver;

    // Lifecycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(getResources().getDimension(R.dimen.toolbar_elevation_default));

        feedView = ((FeedView) findViewById(R.id.feed));

        RecyclerView.LayoutManager feedViewLayoutManager = chooseLayoutManager();
        feedAdapter = new FeedAdapter(null);
        feedView.setAdapter(feedAdapter);
        feedView.setLayoutManager(feedViewLayoutManager);

        View stubView = findViewById(R.id.stub_view);
        feedView.setStubView(stubView);

        refreshLayout = ((SwipeRefreshLayout) findViewById(R.id.refresh_feed_container));
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        getLoaderManager().initLoader(POSTS_LOADER, null, this);
        if (savedInstanceState == null) {
            getSyncHelper().requestSync();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        feedAdapter.setPostClickListener(this);
        if (feedSyncServiceStatusReceiver == null) {
            feedSyncServiceStatusReceiver = new FeedSyncServiceStatusReceiver(this, this);
        }
        feedSyncServiceStatusReceiver.subscribe();
        getSyncHelper().configureSync();
    }

    @Override
    protected void onPause() {
        feedAdapter.unsetPostClickListener();
        if (feedSyncServiceStatusReceiver != null) {
            feedSyncServiceStatusReceiver.unsubscribe();
        }
        getSyncHelper().disableSync();
        super.onPause();
    }
    // End Lifecycle

    // Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int menuItemId = item.getItemId();
        if (menuItemId == R.id.action_show_settings) {
            getRouter().navigateToSettingsActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    // End Menu

    // Loaders
    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new FeedLoader(this, false);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        feedAdapter.setCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        feedAdapter.setCursor(null);
    }
    // End Loaders

    // Listeners
    @Override
    public void onPostClick(Post post) {
        getRouter().navigateToPostActvity(post);
    }

    @Override
    public void onRefresh() {
        getSyncHelper().requestSync();
    }

    @Override
    public void onUpdateProcess() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void onUpdateSuccess(int updates) {
        refreshLayout.setRefreshing(false);
        if (updates != 0) {
            String successMessage = getResources().getString(R.string.feed_snackbar_status_updated);
            showSnackbar(successMessage, Snackbar.LENGTH_SHORT);
        }
    }

    @Override
    public void onUpdateFailure(int status) {
        String message = getResources().getString(R.string.feed_snackbar_status_failure);
        if (status == FeedSyncService.FailureCode.NO_CONNECTION) {
            message = getResources().getString(R.string.feed_snackbar_status_no_internet);
        } else if (status == FeedSyncService.FailureCode.API_UNREACHABLE) {
            message = getResources().getString(R.string.feed_snackbar_status_api_unreachable);
        }
        showSnackbar(message, Snackbar.LENGTH_SHORT);
        refreshLayout.setRefreshing(false);
    }
    // End Listeners

    // Utils
    private void showSnackbar(String message, int duration) {
        if (snackbar != null && snackbar.isShownOrQueued()) {
            snackbar.dismiss();
        }
        snackbar = Snackbar.make(feedView, message, duration);
        snackbar.show();
    }

    private RecyclerView.LayoutManager chooseLayoutManager() {
        if (findViewById(R.id.screen_large_land) != null) {
            int cols = getResources().getInteger(R.integer.post_cols_large_land);
            return new StaggeredGridLayoutManager(cols, StaggeredGridLayoutManager.VERTICAL);
        } else if (findViewById(R.id.screen_xlarge_land) != null) {
            int cols = getResources().getInteger(R.integer.post_cols_xlarge_land);
            return new StaggeredGridLayoutManager(cols, StaggeredGridLayoutManager.VERTICAL);
        } else if (findViewById(R.id.screen_xlarge_port) != null) {
            int cols = getResources().getInteger(R.integer.post_cols_xlarge_port);
            return new StaggeredGridLayoutManager(cols, StaggeredGridLayoutManager.VERTICAL);
        } else {
            return new LinearLayoutManager(this);
        }
    }

    private FeedSyncHelper getSyncHelper() {
        if (syncHelper == null) {
            syncHelper = new FeedSyncHelper(this);
        }
        return syncHelper;
    }

    public SimpleRouter getRouter() {
        if (router == null) {
            router = new SimpleRouter(this);
        }
        return router;
    }
    // End Utils
}
