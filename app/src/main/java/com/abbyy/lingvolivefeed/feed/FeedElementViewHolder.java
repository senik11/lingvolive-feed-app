package com.abbyy.lingvolivefeed.feed;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.abbyy.lingvolivefeed.R;

public class FeedElementViewHolder extends RecyclerView.ViewHolder {
    final CardView postContainer;
    final TextView postHeading;
    final TextView postTranslation;
    final TextView postMessage;

    final int viewType;

    public FeedElementViewHolder(View itemView, int viewType) {
        super(itemView);
        this.viewType = viewType;
        postContainer = ((CardView) itemView.findViewById(R.id.post_card));
        postHeading = ((TextView) itemView.findViewById(R.id.post_heading));
        postTranslation = ((TextView) itemView.findViewById(R.id.post_translation));
        postMessage = ((TextView) itemView.findViewById(R.id.post_message));
    }
}
