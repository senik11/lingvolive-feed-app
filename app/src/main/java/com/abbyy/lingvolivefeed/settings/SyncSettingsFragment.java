package com.abbyy.lingvolivefeed.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;

import com.abbyy.lingvolivefeed.R;

public class SyncSettingsFragment extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener {

    private SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_sync);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        bindPreferenceValue(getPreferenceByKey(R.string.prefs_sync_enabled_key));
        bindPreferenceValue(getPreferenceByKey(R.string.prefs_sync_interval_key));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object o) {
        if (preference.getKey().equals(getString(R.string.prefs_sync_interval_key))) {
            String entry = ((ListPreference) preference).getEntry().toString();
            preference.setSummary(entry);
        }
        return true;
    }

    private void bindPreferenceValue(Preference preference) {
        preference.setOnPreferenceChangeListener(this);
        if (preference.getKey().equals(getString(R.string.prefs_sync_enabled_key))) {
            boolean savedValue = sharedPreferences.getBoolean(preference.getKey(), true);
            onPreferenceChange(preference, savedValue);
        }
        if (preference.getKey().equals(getString(R.string.prefs_sync_interval_key))) {
            String defaultValue = getResources().getString(R.string.prefs_sync_interval_default);
            String savedValue = sharedPreferences.getString(preference.getKey(), defaultValue);
            onPreferenceChange(preference, savedValue);
        }
    }

    private Preference getPreferenceByKey(int keyResource) {
        return findPreference(getString(keyResource));
    }

}
